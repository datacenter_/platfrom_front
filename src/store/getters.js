const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs,
  userId: state => state.user.id,
  authStep: state => state.user.authStep,
  authList: state => state.user.authList,
  country: state => state.optionUse.country,
  allcountry: state => state.optionUse.allcountry,
  games: state => state.optionUse.games,
  allgames: state => state.optionUse.allgames,
  clanlist: state => state.optionUse.clanlist,
  enum_origin_list: state => state.optionUse.enum_origin_list,
  getCountryById: state => (id) => {
    if (state.optionUse.countryMap[id]) {
      if (state.optionUse.countryMap[id].id === id) {
        return state.optionUse.countryMap[id]['name']
      }
    }
  },
  originImage: state => (id) => {
    if (state.optionUse.enum_origin_list[id]) {
      if (state.optionUse.enum_origin_list[id].id === id) {
        return state.optionUse.enum_origin_list[id]['image']
      }
    }
  }
}
export default getters
