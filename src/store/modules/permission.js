import { asyncRoutes, constantRoutes } from '@/router'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  },
  generateRoutesWithAuth({ commit }, data) {
    const { roles, authList } = data
    return new Promise(resolve => {
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      // 设置路由+权限
      let accessRouteNew = []
      if (roles.indexOf('platform') !== -1) {
        accessedRoutes.forEach(item => {
          const pathArr = item.path.substr(1).split('/')
          // console.log(pathArr)
          if (['invoice', 'partTimeJob', 'base', 'pageDesign', 'banner'].indexOf(pathArr[1]) !== -1) {
            const paStr = pathArr.join('|') + '|read'
            if (authList.indexOf(paStr) !== -1) {
              accessRouteNew.push(item)
            }
          } else {
            accessRouteNew.push(item)
          }
        })
      } else {
        accessRouteNew = accessedRoutes
      }

      commit('SET_ROUTES', accessRouteNew)
      resolve(accessRouteNew)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
