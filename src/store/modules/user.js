import { login, logout, getInfo } from '@/api/admin'
import { getToken, setToken, removeToken } from '@/utils/auth'
import router, { resetRouter } from '@/router'

const state = {
  token: getToken(),
  id: '',
  name: '',
  avatar: '',
  introduction: '',
  roles: [],
  authStep: 6,
  comAuth: true,
  authList: []
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
    setToken(token)
  },
  SET_INTRODUCTION: (state, introduction) => {
    state.introduction = introduction
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_STEP: (state, step) => {
    state.authStep = step
  },
  SET_COM_AUTH: (state, c) => {
    state.comAuth = c
  },
  SET_USER_ID: (state, id) => {
    state.id = id
  },
  SET_AUTH_LIST: (state, authList) => {
    state.authList = authList
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const { data } = response
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data, auth, comAuth } = response

        if (!data) {
          reject('Verification failed, please Login again.')
        }
        console.log('-----data----')
        // console.log(data)
        let setp = 6
        if (auth && auth.step) {
          setp = Number(auth.step)
        }

        let c = false
        if (comAuth) {
          c = comAuth > 0
        }
        // const { name, avatar, introduction } = data
        const { role_type, name, id, authList } = data
        console.log('adsfdf', data)
        const rolesList = {
          '1': 'company',
          '2': '管理员',
          '3': '游客',
          '4': 'invoice',
          '5': 'distributor'
        }
        const roleTmp = rolesList[role_type]
        // roles must be a non-empty array
        // if (!roles || roles.length <= 0) {
        //   reject('getInfo: roles must be a non-null array!')
        // }
        // console.log([roleTmp])
        commit('SET_USER_ID', id)
        commit('SET_ROLES', [roleTmp])
        commit('SET_NAME', name)
        commit('SET_AVATAR', '')
        commit('SET_INTRODUCTION', '')
        commit('SET_STEP', setp)
        commit('SET_COM_AUTH', c)
        commit('SET_AUTH_LIST', authList)
        data['roles'] = [roleTmp]
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        commit('SET_STEP', 0)
        commit('SET_COM_AUTH', false)
        commit('SET_AUTH_LIST', [])
        removeToken()
        resetRouter()

        // reset visited views and cached views
        // to fixed https://github.com/PanJiaChen/vue-element-admin/issues/2485
        dispatch('tagsView/delAllViews', null, { root: true })

        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  },

  // dynamically modify permissions
  changeRoles({ commit, dispatch }, role) {
    return new Promise(async resolve => {
      const token = role + '-token'

      commit('SET_TOKEN', token)
      setToken(token)

      const { roles } = await dispatch('getInfo')

      resetRouter()

      // generate accessible routes map based on roles
      const accessRoutes = await dispatch('permission/generateRoutes', roles, { root: true })

      // dynamically add accessible routes
      router.addRoutes(accessRoutes)

      // reset visited views and cached views
      dispatch('tagsView/delAllViews', null, { root: true })

      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
