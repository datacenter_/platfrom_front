import { get_country } from '@/api/clan'
import { get_games } from '@/api/common'
import { get_clan_list } from '@/api/clan'
import { enum_origin } from '@/api/enum'

const state = {
  country: '',
  allcountry: '',
  countryMap: '',
  games: '',
  allgames: '',
  clanlist: '',
  enum_origin_list: ''
}

const mutations = {
  SET_COUNTRY: (state, country) => {
    state.country = country
  },
  SET_COUNTRYS: (state, countrys) => {
    state.allcountry = countrys
  },
  SET_COUNTRY_MAP: (state, countryMap) => {
    state.countryMap = countryMap
  },
  SET_GAMES: (state, games) => {
    state.games = games
  },
  SET_ALLGAMES: (state, allgames) => {
    state.allgames = allgames
  },
  // 所属俱乐部
  SET_CLAN: (state, clanlist) => {
    state.clanlist = clanlist
  },
  // 使用数据源
  SET_ENUM: (state, list) => {
    state.enum_origin_list = list
  }
}

const actions = {
  getCountry({ commit, params }) {
    return new Promise((resolve, reject) => {
      get_country({ params }).then(response => {
        var all = [{ 'id': '', 'name': '全部', 'e_name': 'all' }]
        var k = response.data
        const c = all.concat(k)
        const { data } = response
        const countryMap = {}
        k.forEach(item => {
          countryMap[item['id']] = item
        })
        commit('SET_COUNTRY', data)
        commit('SET_COUNTRYS', c)
        commit('SET_COUNTRY_MAP', countryMap)
        resolve(data, c)
      }).catch(error => {
        reject(error)
      })
    })
  },
  getGames({ commit, params }) {
    return new Promise((resolve, reject) => {
      get_games({ params }).then(response => {
        var all = [{ 'id': '', 'name': '全部', 'e_name': 'all' }]
        var k = response.data
        const c = all.concat(k)
        const { data } = response
        commit('SET_GAMES', data)
        commit('SET_ALLGAMES', c)
        resolve(data, c)
      }).catch(error => {
        reject(error)
      })
    })
  },
  getClan({ commit, params }) {
    return new Promise((resolve, reject) => {
      get_clan_list({ params }).then(response => {
        const { data } = response
        commit('SET_CLAN', data)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  getEnum({ commit, params }) {
    return new Promise((resolve, reject) => {
      enum_origin({ params }).then(response => {
        var k = response.data
        console.log('k')
        console.log(k)
        var tmp = {}
        k.forEach(item => {
          tmp[item['id']] = item
        })
        commit('SET_ENUM', tmp)
        // resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
