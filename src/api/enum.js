import request from '@/utils/request'
// 数据源
export function enum_origin(data) {
  return request({
    url: '/common/enum/origin',
    method: 'get',
    params: data
  })
}
