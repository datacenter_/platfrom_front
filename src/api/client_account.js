import request from '@/utils/request'
// 添加账号
export function clientAdd(data) {
  return request({
    url: '/account/client-account/add',
    method: 'post',
    data
  })
}

// 账号列表
export function client_list(data) {
  return request({
    url: '/account/client-account/list',
    method: 'get',
    params: data
  })
}
// 账号修改
export function client_edit(data) {
  return request({
    url: '/account/client-account/edit',
    method: 'post',
    data
  })
}
// 账号详情
export function client_detail(data) {
  return request({
    url: '/account/client-account/detail',
    method: 'get',
    params: data
  })
}
// 密码修改
export function client_password(data) {
  return request({
    url: '/account/client-account/re-password',
    method: 'post',
    data
  })
}
