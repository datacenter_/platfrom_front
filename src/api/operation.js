import request from '@/utils/request'
// 操作详情
export function operation_log_list(data) {
  return request({
    url: '/operation/log/list',
    method: 'get',
    params: data
  })
}
