import request from '@/utils/request'
// 俱乐部列表
export function clanList(data) {
  return request({
    url: '/org/clan/list',
    method: 'get',
    params: data
  })
}

// 添加/修改-俱乐部
export function clanAdd(data) {
  return request({
    url: '/org/clan/add',
    method: 'post',
    data
  })
}
// 编辑-俱乐部
export function clanInfo(data) {
  return request({
    url: '/org/clan/get-info-by-id',
    method: 'post',
    data
  })
}

// 获取国家
export function get_country(data) {
  return request({
    url: '/org/clan/get-country',
    method: 'get',
    params: data
  })
}

// 删除俱乐部
export function clanDel(data) {
  return request({
    url: '/org/clan/del',
    method: 'post',
    data
  })
}

// 获取修改俱乐部下的战队
export function get_teams(data) {
  return request({
    url: '/org/clan/get-teams',
    method: 'post',
    data
  })
}
// 获取所有俱乐部
export function get_clan_list(data) {
  return request({
    url: '/org/clan/get-clan-list',
    method: 'get',
    params: data
  })
}
