import request from '@/utils/request'
// 赛事列表
export function tournamentList(data) {
  return request({
    url: '/tournament/tournament/list',
    method: 'get',
    params: data
  })
}

// 删除赛事
export function tournamentDel(data) {
  return request({
    url: '/tournament/tournament/del',
    method: 'post',
    data
  })
}

// 添加编辑赛事，阶段，目录，子赛事，分组 新
export function tournamentEdit(data) {
  return request({
    url: '/tournament/tournament/edit',
    method: 'post',
    data
  })
}

// 赛事详情
export function tournamentInfo(data) {
  return request({
    url: '/tournament/tournament/info',
    method: 'get',
    params: data
  })
}
// 详情赛事，阶段，目录，子赛事，分组 新
export function tournamentDetail(data) {
  return request({
    url: '/tournament/tournament/detail',
    method: 'get',
    params: data
  })
}

// 添加阶段赛事
export function tournamentStage(data) {
  return request({
    url: '/tournament/tournament/create-stage',
    method: 'post',
    data
  })
}

// 获取赛事树
export function tournamentTree(data) {
  return request({
    url: '/tournament/tournament/get-relation-tree',
    method: 'get',
    params: data
  })
}

// 添加分组赛事
export function tournamentGroup(data) {
  return request({
    url: '/tournament/tournament/create-group',
    method: 'post',
    data
  })
}
// 获取子赛事详情
export function get_son_tournament(data) {
  return request({
    url: '/tournament/tournament/get-son-tournament',
    method: 'get',
    params: data
  })
}
// 获取阶段详情
export function get_stage(data) {
  return request({
    url: '/tournament/tournament/get-stage',
    method: 'get',
    params: data
  })
}
// 获取分组详情
export function get_group(data) {
  return request({
    url: '/tournament/tournament/get-group',
    method: 'get',
    params: data
  })
}
// 删除子赛事
export function del_son_tournament(data) {
  return request({
    url: '/tournament/tournament/del-son-tournament',
    method: 'post',
    data
  })
}
// 删除阶段
export function del_stage(data) {
  return request({
    url: '/tournament/tournament/del-stage',
    method: 'post',
    data
  })
}
// 删除分组
export function del_group(data) {
  return request({
    url: '/tournament/tournament/del-group',
    method: 'post',
    data
  })
}
// 添加目录赛事
export function create_catalog(data) {
  return request({
    url: '/tournament/tournament/create-catalog',
    method: 'post',
    data
  })
}
// 获取各个类型下战队
export function get_tournament_team(data) {
  return request({
    url: '/tournament/tournament/get-tournament-team',
    method: 'get',
    params: data
  })
}
// 更新战队快照
export function update_team_snapshot(data) {
  return request({
    url: '/tournament/tournament/update-team-snapshot',
    method: 'post',
    data
  })
}
// 移除战队快照
export function remove_team_snapshot(data) {
  return request({
    url: '/tournament/tournament/remove-team-snapshot',
    method: 'post',
    data
  })
}
// 获取战队快照
export function get_team_snapshot(data) {
  return request({
    url: '/tournament/tournament/get-team-snapshot',
    method: 'get',
    params: data
  })
}
// 赛事列表中比赛设置
export function match_resource_config(data) {
  return request({
    url: '/data/config/set-match-resource-config',
    method: 'post',
    data
  })
}
// 编辑战队快照战队信息
export function team_snapshot_edit(data) {
  return request({
    url: '/tournament/tournament/team-snapshot-edit    ',
    method: 'post',
    data
  })
}
// 战队快照详情
export function team_snapshot_detail(data) {
  return request({
    url: '/tournament/tournament/team-detail-snapshot',
    method: 'get',
    params: data
  })
}
// 编辑战队快照选手信息
export function player_snapshot_add(data) {
  return request({
    url: '/tournament/tournament/player-snapshot-add',
    method: 'post',
    data
  })
}
