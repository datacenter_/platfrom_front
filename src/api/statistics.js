import request from '@/utils/request'
import store from '../store'

function getStToken() {
  if ((!localStorage.getItem('view_token'))) {
    const code = randomString(32)
    localStorage.setItem('view_token', code)
  }
  return localStorage.getItem('view_token')
}

function randomString(len) {
  len = len || 32
  var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
  var maxPos = $chars.length
  var pwd = ''
  for (let i = 0; i < len; i++) {
    pwd += $chars.charAt(Math.floor(Math.random() * maxPos))
  }
  return pwd
}

export function addView(url) {
  // 访问页面时监控
  const userId = store.getters.userId
  const token = getStToken()
  const data = {
    user_id: userId,
    page: url,
    token: token,
    self_token: userId > 0 ? userId : token
  }
  return request({
    url: '/statistics/page/view',
    method: 'post',
    data
  })
}

