import request from '@/utils/requestUser'
// rest对局详情
export function battle_list(data) {
  return request({
    url: '/battles/list',
    method: 'get',
    params: data
  })
}
// rest对局事件
export function battle_events(data) {
  return request({
    url: '/battles/events',
    method: 'get',
    params: data
  })
}
// rest道具时间线
export function battle_items(data) {
  return request({
    url: '/battles/items',
    method: 'get',
    params: data
  })
}
// rest技能时间线
export function battle_abilities(data) {
  return request({
    url: '/battles/abilities',
    method: 'get',
    params: data
  })
}
