import request from '@/utils/request'
// 一周比赛数据统计
export function weekData(data) {
  return request({
    url: '/match/match/get-match-count-by-time',
    method: 'GET',
    params: data
  })
}
// 比赛数据统计
export function countData(data) {
  return request({
    url: '/match/match/get-match-count-lately',
    method: 'GET',
    params: data
  })
}
// 手动待更新列表
export function toDoList(data) {
  return request({
    url: '/data/to-do/list',
    method: 'GET',
    params: data
  })
}
// 手动待绑定列表
export function toDoBinding(data) {
  return request({
    url: '/data/binding/to-do-binding',
    method: 'GET',
    params: data
  })
}
// 首页数据源绑定概况
export function bindingDada(data) {
  return request({
    url: '/data/binding/binding-statistics',
    method: 'GET',
    params: data
  })
}
//实时数据量
export function realTimeData(data) {
  return request({
    url: '/match/match/real-time-data',
    method: 'GET',
    params: data
  })
}
//比赛战队异常数据
export function errData(data) {
  return request({
    url: '/match/match/match-many-team',
    method: 'GET',
    params: data
  })
}
export function edit_errData(data) {
  return request({
    url: '/match/match/edit-error-team-by-match',
    method: 'post',
    data
  })
}
