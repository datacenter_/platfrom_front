import request from '@/utils/request'
// 新键比赛
export function match_add(data) {
  return request({
    url: '/match/match/add',
    method: 'post',
    data
  })
}
// 删除比赛
export function match_del(data) {
  return request({
    url: '/v1/match/match/del',
    method: 'post',
    data
  })
}
// 获取赛事
export function get_tournament(data) {
  return request({
    url: '/match/match/get-tournament',
    method: 'get',
    params: data
  })
}
// 获取子赛事
export function get_son_tournament(data) {
  return request({
    url: '/match/match/get-son-tournament',
    method: 'get',
    params: data
  })
}
// 获取阶段
export function get_stage(data) {
  return request({
    url: '/match/match/get-stage',
    method: 'get',
    params: data
  })
}
// 获取分组
export function get_group(data) {
  return request({
    url: '/match/match/get-group',
    method: 'get',
    params: data
  })
}
// 比赛列表
export function get_match_list(data) {
  return request({
    url: '/match/match/list',
    method: 'get',
    params: data
  })
}
// 删除比赛
export function get_match_del(data) {
  return request({
    url: '/match/match/del',
    method: 'post',
    data
  })
}
// 搜索战队
export function get_search_team(data) {
  return request({
    url: '/match/match/search-team',
    method: 'get',
    params: data
  })
}
// 通用录入
export function create_input(data) {
  return request({
    url: '/match/match/create-input',
    method: 'post',
    data
  })
}
// 英雄联盟或刀塔录入
export function game_input(data) {
  return request({
    url: '/match/match/game-input',
    method: 'post',
    data
  })
}
// 获取录入信息
export function get_input_info(data) {
  return request({
    url: '/match/match/get-input',
    method: 'get',
    params: data
  })
}
// 获取比赛详情
export function get_match_detail(data) {
  return request({
    url: '/match/match/detail',
    method: 'get',
    params: data
  })
}// 编辑比赛
export function get_match_edit(data) {
  return request({
    url: '/match/match/edit',
    method: 'post',
    data
  })
}
// ban-pick 选英雄
export function get_hero_list(data) {
  return request({
    url: '/match/match/get-hero-list',
    method: 'get',
    params: data
  })
}
// 比赛录入_获取详情
export function get_realtime_detail(data) {
  return request({
    url: '/match/match/realtime-detail',
    method: 'get',
    params: data
  })
}
// 比赛录入_设置属性
export function get_realtime_set(data) {
  return request({
    url: '/match/match/realtime-set',
    method: 'post',
    data
  })
}// 比赛录入_重抓
export function get_refresh_match(data) {
  return request({
    url: '/match/match/refresh-match',
    method: 'post',
    data
  })
}
// battle列表
export function get_battle_list(data) {
  return request({
    url: '/match/battle/list',
    method: 'get',
    params: data
  })
}
// battle_添加新的
export function get_battle_add(data) {
  return request({
    url: '/match/battle/add',
    method: 'post',
    data
  })
}
// battle_修改
export function get_battle_edit(data) {
  return request({
    url: '/match/battle/edit',
    method: 'post',
    data
  })
}
// battle_详情
export function get_battle_detail(data) {
  return request({
    url: '/match/battle/detail',
    method: 'get',
    params: data
  })
}
// battle_删除
export function get_battle_del(data) {
  return request({
    url: '/match/battle/del',
    method: 'post',
    data
  })
}
// 日志分拣
export function get_sortlog_list(data) {
  return request({
    url: '/match/sortlog/list',
    method: 'get',
    params: data
  })
}
// 日志分拣比赛查询
export function get_sort_match_list(data) {
  return request({
    url: '/match/sortlog/sort-match-list',
    method: 'get',
    params: data
  })
}
// 日志分拣添加
export function get_sortlog_add(data) {
  return request({
    url: '/match/sortlog/sort-add',
    method: 'post',
    data
  })
}
// 对照详情_match
export function task_contrast_match(data) {
  return request({
    url: '/task/contrast/match',
    method: 'get',
    params: data
  })
}
// SortingLog - 单条数据
export function sortlog_info(data) {
  return request({
    url: '/match/sortlog/sort-select',
    method: 'get',
    params: data
  })
}
// SortingLog-修改
export function get_sortlog_edit(data) {
  return request({
    url: '/match/sortlog/sort-edit',
    method: 'post',
    data
  })
}
// SortingLog-删除
export function get_sortlog_delete(data) {
  return request({
    url: '/match/sortlog/sort-delete',
    method: 'post',
    data
  })
}
// 获取比赛选手和战队相关信息
export function get_log_info(data) {
  return request({
    url: '/match/match/get-info',
    method: 'get',
    params: data
  })
}
// 视频源列表
export function get_stream_list(data) {
  return request({
    url: '/match/stream/list',
    method: 'get',
    params: data
  })
}
// 视频源新建编辑
export function get_stream_edit(data) {
  return request({
    url: '/match/stream/edit',
    method: 'post',
    data
  })
}
// 视频源删除
export function get_stream_del(data) {
  return request({
    url: '/match/stream/del',
    method: 'post',
    data
  })
}
// 可用视频源
export function live_stream_list(data) {
  return request({
    url: '/match/live-stream/list',
    method: 'get',
    params: data
  })
}
// 可用添加视频源
export function live_stream_add(data) {
  return request({
    url: '/match/live-stream/add',
    method: 'post',
    data
  })
}
// 视频源详情
export function get_stream_detail(data) {
  return request({
    url: '/match/stream/detail',
    method: 'get',
    params: data
  })
}
// socket列表
export function socket_list(data) {
  return request({
    url: '/match/socketpt/list',
    method: 'get',
    params: data
  })
}
// socket详情
export function socket_detail(data) {
  return request({
    url: '/match/socketpt/detail',
    method: 'get',
    params: data
  })
}
// socket编辑添加
export function socket_edit(data) {
  return request({
    url: '/match/socketpt/edit',
    method: 'post',
    data
  })
}
// socket删除
export function socket_del(data) {
  return request({
    url: '/match/socketpt/del',
    method: 'post',
    data
  })
}
// 直播平台列表
export function platform_list(data) {
  return request({
    url: '/match/stream/get-stream-platform',
    method: 'get',
    params: data
  })
}
// 搜索游戏下所有赛事
// export function search_tournament_list(data) {
//   return request({
//     url: '/match/match/get-tournament',
//     method: 'get',
//     params: data
//   })
// }
// Round_添加
export function get_round_add(data) {
  return request({
    url: '/match/round/round-add',
    method: 'post',
    data
  })
}
// Round_删除
export function get_round_del(data) {
  return request({
    url: '/match/round/round-del',
    method: 'post',
    data
  })
}
// 5e更新
export function update_prefect_logo(data) {
  return request({
    url: '/match/csgo5eplay-match/update-prefect-logo',
    method: 'post',
    data
  })
}
// 5e重抓
export function do_refresh(data) {
  return request({
    url: '/match/csgo5eplay-match/do-refresh',
    method: 'post',
    data
  })
}
// 恢复battle
export function battle_recovery(data) {
  return request({
    url: '/match/battle/recovery',
    method: 'post',
    data
  })
}
// 录入页面api速率编辑接口
export function data_speed(data) {
  return request({
    url: '/match/match/edit-data-speed',
    method: 'post',
    data
  })
}
// 更新api
export function refresh_battle(data) {
  return request({
    url: '/match/battle/refresh-battle',
    method: 'post',
    data
  })
}
export function receive_list(data) {
  return request({
    url: '/match/socket/receive-list',
    method: 'get',
    params: data
  })
}
export function lived_list(data) {
  return request({
    url: '/match/socketpt/lived-del ',
    method: 'post',
    data
  })
}
