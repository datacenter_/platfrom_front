import request from '@/utils/request'
// lol-英雄列表
export function heroList(data) {
  return request({
    url: '/metadata/lol/hero-list',
    method: 'get',
    params: data
  })
}

// lol-英雄查找
export function getHeroList(data) {
  return request({
    url: '/metadata/lol/get-hero-list',
    method: 'get',
    params: data
  })
}

// lol-英雄添加
export function heroAdd(data) {
  return request({
    url: '/metadata/lol/add-hero',
    method: 'post',
    data
  })
}
// 删除lol-英雄
export function heroDel(data) {
  return request({
    url: '/metadata/lol/del-hero',
    method: 'post',
    data
  })
}
// lol-英雄信息
export function lol_hero_Info(data) {
  return request({
    url: '/metadata/lol/hero-info',
    method: 'get',
    params: data
  })
}
// lol-技能列表
export function lol_ability_list(data) {
  return request({
    url: '/metadata/lol/ability-list',
    method: 'get',
    params: data
  })
}
// lol-技能详情
export function lol_ability_info(data) {
  return request({
    url: '/metadata/lol/ability-info',
    method: 'get',
    params: data
  })
}
// lol-新建、编辑技能
export function lol_ability_add(data) {
  return request({
    url: '/metadata/lol/add-ability',
    method: 'post',
    data
  })
}
// 删除lol-技能
export function lol_ability_del(data) {
  return request({
    url: '/metadata/lol/del-ability',
    method: 'post',
    data
  })
}
// 道具列表-lol
export function lol_prop_list(data) {
  return request({
    url: '/metadata/lol/prop-list',
    method: 'get',
    params: data
  })
}
// 道具详情-lol
export function lol_prop_info(data) {
  return request({
    url: '/metadata/lol/prop-info',
    method: 'get',
    params: data
  })
}
// lol-添加道具
export function lol_add_prop(data) {
  return request({
    url: '/metadata/lol/add-prop',
    method: 'post',
    data
  })
}
// 删除道具-lol
export function lol_del_prop(data) {
  return request({
    url: '/metadata/lol/del-prop',
    method: 'post',
    data
  })
}
// 召唤师技能列表
export function skill_list(data) {
  return request({
    url: '/metadata/lol/skill-list',
    method: 'get',
    params: data
  })
}
// 召唤师技能详情
export function skill_info(data) {
  return request({
    url: '/metadata/lol/skill-info',
    method: 'get',
    params: data
  })
}
// lol-新建召唤师技能
export function lol_add_skill(data) {
  return request({
    url: '/metadata/lol/add-skill',
    method: 'post',
    data
  })
}
// lol-删除召唤师技能
export function lol_del_skill(data) {
  return request({
    url: '/metadata/lol/del-skill',
    method: 'post',
    data
  })
}
// lol-符文列表
export function rune_list(data) {
  return request({
    url: '/metadata/lol/rune-list',
    method: 'get',
    params: data
  })
}
// lol-符文详情
export function rune_info(data) {
  return request({
    url: '/metadata/lol/rune-info',
    method: 'get',
    params: data
  })
}
// lol-添加符文
export function lol_add_rune(data) {
  return request({
    url: '/metadata/lol/add-rune',
    method: 'post',
    data
  })
}
// 删除符文-lol
export function lol_del_rune(data) {
  return request({
    url: '/metadata/lol/del-rune',
    method: 'post',
    data
  })
}
// lol地图查找
export function lol_map_data(data) {
  return request({
    url: '/metadata/lol/map-data',
    method: 'get',
    params: data
  })
}
// 刀塔游戏
// 刀塔英雄列表
export function dota_heroList(data) {
  return request({
    url: '/metadata/dota/hero-list',
    method: 'get',
    params: data
  })
}

// dota-英雄查找
export function dotagetHeroList(data) {
  return request({
    url: '/metadata/dota/get-hero-list',
    method: 'get',
    params: data
  })
}

// 刀塔 一英雄添加
export function dotaAdd(data) {
  return request({
    url: '/metadata/dota/add-hero',
    method: 'post',
    data
  })
}

// 刀塔英雄单条信息
export function heroInfo(data) {
  return request({
    url: '/metadata/dota/hero-info',
    method: 'get',
    params: data
  })
}
// 刀塔-技能列表、详情
export function dota_ability_list(data) {
  return request({
    url: '/metadata/dota/ability-list',
    method: 'get',
    params: data
  })
}
// 刀塔-详情
export function dota_get_info(data) {
  return request({
    url: '/metadata/dota/get-info',
    method: 'get',
    params: data
  })
}
// 刀塔-新建、编辑技能
export function dota_ability_add(data) {
  return request({
    url: '/metadata/dota/add-ability',
    method: 'post',
    data
  })
}
// 刀塔-删除技能
export function dota_del_ability(data) {
  return request({
    url: '/metadata/dota/del-ability',
    method: 'post',
    data
  })
}
// 刀塔-编辑技能
export function dota_ability_edit(data) {
  return request({
    url: '/metadata/dota/edit-ability',
    method: 'post',
    data
  })
}
// 刀塔-天赋列表.详情
export function dota_talent_list(data) {
  return request({
    url: '/metadata/dota/talent-list',
    method: 'get',
    params: data
  })
}
// 刀塔-新建.编辑天赋
export function dota_talent_add(data) {
  return request({
    url: '/metadata/dota/add-talent',
    method: 'post',
    data
  })
}
// 刀塔-删除天赋
export function dota_del_talent(data) {
  return request({
    url: '/metadata/dota/del-talent',
    method: 'post',
    data
  })
}
// 刀塔道具列表
export function prop_list(data) {
  return request({
    url: '/metadata/dota/prop-list',
    method: 'get',
    params: data
  })
}

// 删除刀塔英雄
export function del_hero(data) {
  return request({
    url: '/metadata/dota/del-hero',
    method: 'post',
    data
  })
}
// 刀塔-添加道具
export function dota_propAdd(data) {
  return request({
    url: '/metadata/dota/add-prop',
    method: 'post',
    data
  })
}
// 刀塔——天赋选择
export function choose_talent(data) {
  return request({
    url: '/metadata/dota/choose-talent',
    method: 'get',
    params: data
  })
}
// 刀塔——地图查找
export function Dota_map_data(data) {
  return request({
    url: '/metadata/dota/map-data',
    method: 'get',
    params: data
  })
}
// 删除道具
export function dota_propDel(data) {
  return request({
    url: '/metadata/dota/del-prop',
    method: 'post',
    data
  })
}
// csgo地图列表
export function csgo_map_list(data) {
  return request({
    url: '/metadata/csgo/map-list',
    method: 'get',
    params: data
  })
}
// csgo地图新建编辑
export function csgo_map_edit(data) {
  return request({
    url: '/metadata/csgo/edit-map',
    method: 'post',
    data
  })
}
// csgo地图详情
export function csgo_map_info(data) {
  return request({
    url: '/metadata/csgo/map-info',
    method: 'get',
    params: data
  })
}
// csgo地图删除
export function csgo_map_del(data) {
  return request({
    url: '/metadata/csgo/del-map',
    method: 'post',
    data
  })
}
// csgo武器列表
export function csgo_weapon_list(data) {
  return request({
    url: '/metadata/csgo/weapon-list',
    method: 'get',
    params: data
  })
}
// csgo武器新建编辑
export function csgo_weapon_edit(data) {
  return request({
    url: '/metadata/csgo/edit-weapon',
    method: 'post',
    data
  })
}
// csgo武器详情
export function csgo_weapon_info(data) {
  return request({
    url: '/metadata/csgo/weapon-info',
    method: 'get',
    params: data
  })
}
// csgo武器删除
export function csgo_weapon_del(data) {
  return request({
    url: '/metadata/csgo/del-weapon',
    method: 'post',
    data
  })
}
// csgo图片查找
export function csgo_map_data(data) {
  return request({
    url: '/metadata/csgo/map-data',
    method: 'get',
    params: data
  })
}
