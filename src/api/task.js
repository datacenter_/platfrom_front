import request from '@/utils/request'
// 任务列表
export function task_list(data) {
  return request({
    url: '/task/task-info/list',
    method: 'get',
    params: data
  })
}
// 对照详情_资源列表
export function contrast_enums(data) {
  return request({
    url: '/task/contrast/enums',
    method: 'get',
    params: data
  })
}
// 对照详情
export function contrast_detail(data) {
  return request({
    url: '/task/contrast/detail',
    method: 'get',
    params: data
  })
}
// 对照详情_刷新脚本
export function contrast_refresh(data) {
  return request({
    url: '/task/contrast/refresh',
    method: 'post',
    data
  })
}
// 获取正式表数据以及关联数据
export function contrast_master_data(data) {
  return request({
    url: '/task/contrast/master-data',
    method: 'post',
    data
  })
}
