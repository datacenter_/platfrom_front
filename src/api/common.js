import request from '@/utils/request'
// 游戏列表
export function get_games(data) {
  return request({
    url: '/common/enum/get-games',
    method: 'get',
    params: data
  })
}

// 选手战队位置
export function get_position(data) {
  return request({
    url: '/common/enum/get-position',
    method: 'get',
    params: data
  })
}

// 选手战队状态
export function get_state(data) {
  return request({
    url: '/common/enum/get-state',
    method: 'get',
    params: data
  })
}

// 赛事状态
export function get_tournament_state(data) {
  return request({
    url: '/common/enum/get-tournament-state',
    method: 'get',
    params: data
  })
}
// 获取首页信息
export function get_data(data) {
  return request({
    url: '/admin/user/get-data',
    method: 'get',
    params: data
  })
}
// 赛制类型
export function get_match_rule(data) {
  return request({
    url: '/common/enum/get-match-rule',
    method: 'get',
    params: data
  })
}
// 解绑
export function close_bing(data) {
  return request({
    url: '/data/data/close-bing',
    method: 'post',
    data
  })
}
// 码表
export function get_enums(data) {
  return request({
    url: '/common/enum/enums',
    method: 'get',
    params: data
  })
}
