import request from '@/utils/request'
// 战队列表
export function teamList(data) {
  return request({
    url: '/org/team/list',
    method: 'get',
    params: data
  })
}

// 添加战队
export function teamAdd(data) {
  return request({
    url: '/org/team/add',
    method: 'post',
    data
  })
}// 添修改战队
export function teamEdit(data) {
  return request({
    url: '/org/team/edit',
    method: 'post',
    data
  })
}

// 战队详情
export function teamInfo(data) {
  return request({
    url: '/org/team/detail',
    method: 'get',
    params: data
  })
}

// 删除战队
export function teamDel(data) {
  return request({
    url: '/org/team/del',
    method: 'post',
    data
  })
}

// 搜索战队
export function search_team(data) {
  return request({
    url: '/org/team/search-team',
    method: 'get',
    params: data
  })
}
// 获取多个战队列表
export function get_teams_info(data) {
  return request({
    url: '/org/team/get-teams-info',
    method: 'get',
    params: data
  })
}
