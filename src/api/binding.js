import request from '@/utils/request'
// team绑定列表
export function binding_team_list(data) {
  return request({
    url: '/data/binding-team/list',
    method: 'get',
    params: data
  })
}
// 绑定用team绑定
export function binding_team_binding(data) {
  return request({
    url: '/data/binding-team/binding',
    method: 'post',
    data
  })
}
// 绑定用team添加
export function binding_team_add(data) {
  return request({
    url: '/data/binding-team/add',
    method: 'post',
    data
  })
}
// 绑定用team详情
export function binding_team_detail(data) {
  return request({
    url: '/data/binding-team/detail',
    method: 'get',
    params: data
  })
}
// 绑定用选手列表
export function binding_player_list(data) {
  return request({
    url: '/data/binding-player/list',
    method: 'get',
    params: data
  })
}
// 绑定用选手绑定
export function binding_player_binding(data) {
  return request({
    url: '/data/binding-player/binding',
    method: 'post',
    data
  })
}
// 绑定用选手详情
export function binding_player_detail(data) {
  return request({
    url: '/data/binding-player/detail',
    method: 'get',
    params: data
  })
}
// 绑定用选手添加
export function binding_player_add(data) {
  return request({
    url: '/data/binding-player/add',
    method: 'post',
    data
  })
}
// metadata_列表
export function binding_metadata_list(data) {
  return request({
    url: '/data/binding-metadata/list',
    method: 'get',
    params: data
  })
}
// medatadata_绑定_自动添加
export function binding_metadata_add(data) {
  return request({
    url: '/data/binding-metadata/add',
    method: 'post',
    data
  })
}
// metadata_绑定_按照推荐绑定
export function binding_metadata_binding(data) {
  return request({
    url: '/data/binding-metadata/binding',
    method: 'post',
    data
  })
}
// metadata_相似绑定详情
export function binding_metadata_detail(data) {
  return request({
    url: '/data/binding-metadata/detail',
    method: 'get',
    params: data
  })
}
// 赛事绑定列表
export function binding_tournament_list(data) {
  return request({
    url: '/data/binding-tournament/list',
    method: 'get',
    params: data
  })
}
// 赛事_绑定_自动添加
export function binding_tournament_add(data) {
  return request({
    url: '/data/binding-tournament/add',
    method: 'post',
    data
  })
}
// 赛事_绑定_按照推荐绑定
export function binding_tournament_binding(data) {
  return request({
    url: '/data/binding-tournament/binding',
    method: 'post',
    data
  })
}
// 赛事树形状绑定
export function binding_tournament_tree_list(data) {
  return request({
    url: '/data/binding-tournament/tree-list',
    method: 'get',
    params: data
  })
}
// 赛事树=》子树绑定
export function binding_tournament_bind_son(data) {
  return request({
    url: '/data/binding-tournament/bind-son',
    method: 'post',
    data
  })
}
// 赛事下级绑定新
export function binding_tournament_trees_list(data) {
  return request({
    url: '/tournament/tournament/trees-list',
    method: 'get',
    params: data
  })
}
// 解绑
export function binding_tournament_close_bind(data) {
  return request({
    url: '/data/binding-tournament/close-bind',
    method: 'post',
    data
  })
}
// 比赛绑定列表
export function binding_match_list(data) {
  return request({
    url: '/data/binding-match/list',
    method: 'get',
    params: data
  })
}
// 比赛_绑定_自动添加
export function binding_match_add(data) {
  return request({
    url: '/data/binding-match/add',
    method: 'post',
    data
  })
}
// 比赛_绑定_按照推荐绑定
export function binding_match_binding(data) {
  return request({
    url: '/data/binding-match/binding',
    method: 'post',
    data
  })
}
// 获取绑定信息
export function get_binding_info(data) {
  return request({
    url: '/data/data/get-bing-info',
    method: 'get',
    params: data
  })
}
// 绑定用tournament详情
export function binding_tournament_detail(data) {
  return request({
    url: '/data/binding-tournament/detail',
    method: 'get',
    params: data
  })
}
// 绑定用match详情
export function binding_match_detail(data) {
  return request({
    url: '/data/binding-match/detail',
    method: 'get',
    params: data
  })
}
// 单抓重置
export function rese_stand(data) {
  return request({
    url: '/data/binding-match/reset-stand',
    method: 'post',
    data
  })
}
