import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/admin/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/admin/user/get-auth-info',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/admin/user/logout',
    method: 'get'
  })
}

export function list(data) {
  return request({
    url: '/admin/admin-manager/list',
    method: 'GET',
    params: data
  })
}
export function permession(data) {
  return request({
    url: '/admin/admin-manager/get-permission',
    method: 'get',
    params: data
  })
}

export function update(data) {
  return request({
    url: '/admin/admin-manager/update',
    method: 'post',
    data
  })
}
export function codeList(data) {
  return request({
    url: '/admin/admin-manager/re-password',
    method: 'post',
    data
  })
}
export function deleteds(data) {
  return request({
    url: '/admin/admin-manager/delete',
    method: 'post',
    data
  })
}
