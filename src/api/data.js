import request from '@/utils/request'
// 默认配置修改
export function config_edit(data) {
  return request({
    url: '/data/config/edit',
    method: 'post',
    data
  })
}
// 默认配置列表
export function get_config_list(data) {
  return request({
    url: '/data/config/list',
    method: 'get',
    params: data
  })
}
// 默认配置详情
export function get_config_detail(data) {
  return request({
    url: '/data/config/detail',
    method: 'get',
    params: data
  })
}
// 手动待处理列表
export function data_change_list(data) {
  return request({
    url: '/data/to-do/list',
    method: 'get',
    params: data
  })
}
// 手动待处理详情
export function data_change_detail(data) {
  return request({
    url: '/data/to-do/detail',
    method: 'get',
    params: data
  })
}
// 批量修改资源绑定
export function config_batch(data) {
  return request({
    url: '/data/config/batch',
    method: 'post',
    data
  })
}
// 资源配置详情
export function resource_config_detail(data) {
  return request({
    url: '/data/config/resource-config-detail',
    method: 'get',
    params: data
  })
}
// 资源配置修改
export function resource_config_edit(data) {
  return request({
    url: '/data/config/resource-config-edit',
    method: 'post',
    data
  })
}
// 数据更新debug
export function debug_add(data) {
  return request({
    url: '/data/to-do/debug-add',
    method: 'post',
    data
  })
}
// 手动待处理详情
export function to_do_detail(data) {
  return request({
    url: '/data/to-do/deal',
    method: 'post',
    data
  })
}
// 刷新单条数据
export function refresh_stand(data) {
  return request({
    url: '/data/binding-match/refresh-stand',
    method: 'post',
    data
  })
}
export function regrasp_socket(data) {
  return request({
    url: '/match/socketpt/regrasp-socket',
    method: 'post',
    data
  })
}
