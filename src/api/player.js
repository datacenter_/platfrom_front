import request from '@/utils/request'
// 选手列表
export function playerList(data) {
  return request({
    url: '/org/player/list',
    method: 'get',
    params: data
  })
}

// 添加修改选手
export function playerAdd(data) {
  return request({
    url: '/org/player/edit',
    method: 'post',
    data
  })
}

// 选手详情
export function playerInfo(data) {
  return request({
    url: '/org/player/detail',
    method: 'get',
    params: data
  })
}

// 选手删除
export function playerDel(data) {
  return request({
    url: '/org/player/del',
    method: 'post',
    data
  })
}

// 搜索选手
export function search_player(data) {
  return request({
    url: '/org/player/search-player',
    method: 'get',
    params: data
  })
}
