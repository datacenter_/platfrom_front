import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'
import { sync } from 'vuex-router-sync'
import validator from 'UTIL/validator'
import 'UTIL/filters'
import toast from 'COMMON/toast/toast.js'
Vue.use(validator)
Vue.use(toast)
import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log
import * as echarts from 'echarts'
import VueQuillEditor from 'vue-quill-editor'

import dataV from '@jiaminghi/data-view'
Vue.use(dataV)

// 适配flex
import '@/common/flexible.js'

Vue.prototype.$echarts = echarts
import * as filters from './filters' // global filters
sync(store, router)
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */
import { mockXHR } from '../mock'
if (process.env.NODE_ENV === 'production') {
  mockXHR()
}
Vue.use(VueQuillEditor)
Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false
import shaoXia from './utils/shaoXia.js'
Vue.prototype.$shaoXia = shaoXia
import yangJian from '@/utils/yangJian'
Vue.prototype.$yangJian = yangJian
Vue.prototype.$hasAuth = (authUrl) => {
  // 平台端判断权限
  if (store.getters.roles.indexOf('platform') === -1) {
    return true
  }
  if (store.getters.authList.indexOf(authUrl) !== -1) {
    return true
  }
}
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
