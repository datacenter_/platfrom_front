import Layout from '@/layout'
const gamecenterRouters = [
  // {
  //   path: '/gamecenter/tableau',
  //   component: Layout,
  //   redirect: 'gamecenter/tableau',
  //   name: 'tableau',
  //   meta: {
  //     title: '大数据管理',
  //     icon: 'tableau'
  //     // hidden: true
  //   },
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/gamecenter/tableau/index'),
  //       name: 'tableau_index',
  //       meta: { title: '大数据管理', noCache: true }
  //       // hidden: true
  //     }
  //   ]
  // },
  {
    path: '/gamecenter/clan',
    component: Layout,
    redirect: 'gamecenter/clan/list',
    name: 'gamecenter_clan',
    meta: {
      title: '俱乐部管理',
      icon: 'clan'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'clublist',
        component: () => import('@/views/gamecenter/clan/clublist'),
        name: 'clan_clublist',
        meta: { title: '俱乐部管理', noCache: true }
      },
      {
        path: 'add',
        component: () => import('@/views/gamecenter/clan/add'),
        name: 'clan_add',
        meta: { title: '新建俱乐部', noCache: true },
        hidden: true
      },
      {
        path: 'edit',
        component: () => import('@/views/gamecenter/clan/edit'),
        name: 'clan_edit',
        meta: { title: '编辑俱乐部', noCache: true },
        hidden: true
      }
      // {
      //   path: 'grid',
      //   component: () => import('@/views/gamecenter/clan/test/grid'),
      //   name: 'css_grid',
      //   meta: { title: '测试页面', noCache: true },
      //   hidden: true
      // },
    ]
  },
  {
    path: '/gamecenter/team',
    component: Layout,
    redirect: 'gamecenter/team',
    name: 'gamecenter_team',
    meta: {
      title: '战队管理',
      icon: 'team'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'teaminfo',
        component: () => import('@/views/black'),
        name: 'distributor_teaminfo',
        meta: { title: '战队管理', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/team/teaminfo/list'),
            name: 'teaminfo_list',
            meta: { title: '战队管理', noCache: true }
          },
          {
            path: 'add',
            component: () => import('@/views/gamecenter/team/teaminfo/add'),
            name: 'teaminfo_add',
            meta: { title: '新建战队', noCache: true },
            hidden: true
          },
          {
            path: 'update',
            component: () => import('@/views/gamecenter/team/teaminfo/update'),
            name: 'teaminfo_update',
            meta: { title: '更新', noCache: true },
            hidden: true
          },
          {
            path: 'edit',
            component: () => import('@/views/gamecenter/team/teaminfo/edit'),
            name: 'teaminfo_edit',
            meta: { title: '编辑', noCache: true },
            hidden: true
          },
          {
            path: 'edit_err',
            component: () => import('@/views/gamecenter/team/teaminfo/edit_err'),
            name: 'edit_err',
            meta: { title: '错误处理', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'binding',
        component: () => import('@/views/black'),
        name: 'team_binding',
        meta: { title: '战队绑定', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/team/binding/list'),
            name: 'binding_list',
            meta: { title: '战队绑定', noCache: true }
          },
          {
            path: 'batchadd',
            component: () => import('@/views/gamecenter/team/binding/batchadd'),
            name: 'binding_batchadd',
            meta: { title: '相似绑定', noCache: true },
            hidden: true
          }
        ]
      }
    ]
  },
  {
    path: '/gamecenter/player',
    component: Layout,
    redirect: 'gamecenter/player',
    name: 'gamecenter_player',
    meta: {
      title: '选手管理',
      icon: 'player'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'playerinfo',
        component: () => import('@/views/black'),
        name: 'distributor_playerinfo',
        meta: { title: '选手管理', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/player/playerinfo/list'),
            name: 'playerinfo_list',
            meta: { title: '选手管理', noCache: true }
          },
          {
            path: 'add',
            component: () => import('@/views/gamecenter/player/playerinfo/add'),
            name: 'playerinfo_add',
            meta: { title: '新建选手', noCache: true },
            hidden: true

          },
          {
            path: 'update',
            component: () => import('@/views/gamecenter/player/playerinfo/update'),
            name: 'playerinfo_update',
            meta: { title: '更新', noCache: true },
            hidden: true
          },
          {
            path: 'edit',
            component: () => import('@/views/gamecenter/player/playerinfo/edit'),
            name: 'playerinfo_edit',
            meta: { title: '编辑', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'binding',
        component: () => import('@/views/black'),
        name: 'distributor_binding',
        meta: { title: '选手绑定', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/player/binding/list'),
            name: 'player_list',
            meta: { title: '选手绑定', noCache: true }
          },
          {
            path: 'batchadd',
            component: () => import('@/views/gamecenter/player/binding/batchadd'),
            name: 'player_batchadd',
            meta: { title: '相似绑定', noCache: true },
            hidden: true
          }
        ]
      }
    ]
  },
  {
    path: '/gamecenter/tournament',
    component: Layout,
    redirect: 'gamecenter/tournament',
    name: 'gamecenter_tournament',
    meta: {
      title: '赛事管理',
      icon: 'tournament'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'groupTemplate',
        component: () => import('@/views/black'),
        name: 'distributor_groupTemplate',
        meta: { title: '分组模版默认设置', inoCache: true },
        hidden: true,
        children: [
          {
            path: 'template_info',
            component: () => import('@/views/gamecenter/tournament/groupTemplate/template_info'),
            name: 'groupTemplate_template',
            meta: { title: '分组模版默认设置', noCache: true }
          },
          {
            path: 'default',
            component: () => import('@/views/gamecenter/tournament/groupTemplate/default'),
            name: 'groupTemplate_default',
            meta: { title: '设置默认值', noCache: true }
          }
        ]
      },
      {
        path: 'tournamentInfo',
        component: () => import('@/views/black'),
        name: 'tournamentInfo',
        meta: { title: '赛事管理', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/list'),
            name: 'tournamentInfo_list',
            meta: { title: '赛事管理', noCache: true }
          },
          {
            path: 'default',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/default'),
            name: 'tournamentInfo_default',
            meta: { title: '更新设置', noCache: true },
            hidden: true
          },
          {
            path: 'default_tournament',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/default_tournament'),
            name: 'tournamentInfo_default_tournament',
            meta: { title: '赛事更新设置', noCache: true },
            hidden: true
          },
          {
            path: 'add',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/add'),
            name: 'tournamentInfo_add',
            meta: { title: '新建赛事', noCache: true },
            hidden: true
          },
          {
            path: 'edit',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/edit'),
            name: 'tournamentInfo_add',
            meta: { title: '编辑', noCache: true },
            hidden: true
          },
          {
            path: 'photo',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/photo'),
            name: 'tournamentInfo_photo',
            meta: { title: '战队快照', noCache: true },
            hidden: true
          },
          {
            path: 'photo_edit',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/photo_edit'),
            name: 'tournamentInfo_photo_edit',
            meta: { title: '编辑战队快照', noCache: true },
            hidden: true
          },
          {
            path: 'fight',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/fight'),
            name: 'tournamentInfo_fight',
            meta: { title: '对战图', noCache: true },
            hidden: true
          },
          {
            path: 'warMap',
            component: () => import('@/views/gamecenter/tournament/tournamentInfo/warMap'),
            name: 'tournamentInfo_warMap',
            meta: { title: '对战图1', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'binding',
        component: () => import('@/views/black'),
        name: 'binding',
        meta: { title: '赛事绑定', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/tournament/binding/list'),
            name: 'tournament_list',
            meta: { title: '赛事绑定', noCache: true }
          },
          {
            path: 'batchadd',
            component: () => import('@/views/gamecenter/tournament/binding/batchadd'),
            name: 'tournament_batchadd',
            meta: { title: '相似绑定', noCache: true },
            hidden: true
          },
          {
            path: 'bindingson',
            component: () => import('@/views/gamecenter/tournament/binding/bindingson'),
            name: 'tournament_bindingson',
            meta: { title: '绑定子项', noCache: true },
            hidden: true
          }
          // {
          //   path: 'outgame',
          //   component: () => import('@/views/gamecenter/tournament/binding/outgame'),
          //   name: 'outgame',
          //   meta: { title: '淘汰赛', noCache: true }
          // }
        ]
      }
    ]
  },
  {
    path: '/gamecenter/match/matchinfo',
    component: Layout,
    redirect: 'gamecenter/match/matchinfo',
    name: 'gamecenter_match',
    meta: {
      title: '比赛管理',
      icon: 'match'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'receive_list',
        component: () => import('@/views/gamecenter/match/matchinfo/receive_list'),
        name: 'receive_list',
        meta: { title: '数据列表', noCache: true },
        hidden: true
      },
      {
        path: 'list',
        component: () => import('@/views/gamecenter/match/matchinfo/list'),
        name: 'matchinfo_list',
        meta: { title: '比赛管理', noCache: true }
      },
      {
        path: 'add',
        component: () => import('@/views/gamecenter/match/matchinfo/add'),
        name: 'matchinfo_add',
        meta: { title: '新建比赛', noCache: true },
        hidden: true
      },
      {
        path: 'update',
        component: () => import('@/views/gamecenter/match/matchinfo/update'),
        name: 'matchinfo_update',
        meta: { title: '更新', noCache: true },
        hidden: true
      },
      {
        path: 'edit',
        component: () => import('@/views/gamecenter/match/matchinfo/edit'),
        name: 'matchinfo_edit',
        meta: { title: '编辑', noCache: true },
        hidden: true
      },
      {
        path: 'write',
        component: () => import('@/views/black'),
        name: 'write',
        meta: { title: '比赛录入', inoCache: true },
        hidden: true,
        children: [
          {
            path: 'info',
            component: () => import('@/views/gamecenter/match/matchinfo/write/info'),
            name: 'write_info',
            meta: { title: '通用比赛录入', noCache: true },
            hidden: true
          },
          {
            path: 'yxinfo',
            component: () => import('@/views/gamecenter/match/matchinfo/write/yxinfo'),
            name: 'write_yxinfo',
            meta: { title: '英雄联盟比赛录入', noCache: true },
            hidden: true
          },
          {
            path: 'dota2',
            component: () => import('@/views/gamecenter/match/matchinfo/write/dota2'),
            name: 'write_dtinfo',
            meta: { title: '刀塔2比赛录入', noCache: true },
            hidden: true
          },
          {
            path: 'fkinfo',
            component: () => import('@/views/gamecenter/match/matchinfo/write/fkinfo'),
            name: 'write_fkinfo',
            meta: { title: '反恐精英比赛录入', noCache: true },
            hidden: true
          },
          {
            path: 'video',
            component: () => import('@/views/gamecenter/match/matchinfo/write/video'),
            name: 'write_video',
            meta: { title: '视频源管理', noCache: true }
          },
          {
            path: 'update',
            component: () => import('@/views/gamecenter/match/matchinfo/write/update'),
            name: 'write_update',
            meta: { title: '更新', noCache: true }
          },
          {
            path: 'websocketInfo',
            component: () => import('@/views/gamecenter/match/matchinfo/write/websocketInfo'),
            name: 'websocketInfo',
            meta: { title: '查看websocket', noCache: true }
          },
          {
            path: 'fxRestApiInfo',
            component: () => import('@/views/gamecenter/match/matchinfo/write/fxRestApiInfo'),
            name: 'fxRestApiInfo',
            meta: { title: '查看restApi', noCache: true }
          },
          {
            path: 'yxRestApiInfo',
            component: () => import('@/views/gamecenter/match/matchinfo/write/yxRestApiInfo'),
            name: 'yxRestApiInfo',
            meta: { title: '查看restApi', noCache: true }
          },
          {
            path: 'dota2Websocket',
            component: () => import('@/views/gamecenter/match/matchinfo/write/dota2Websocket'),
            name: 'dota2Websocket',
            meta: { title: '查看websocket', noCache: true }
          },
          {
            path: 'dota2RestApi',
            component: () => import('@/views/gamecenter/match/matchinfo/write/dota2RestApi'),
            name: 'dota2RestApi',
            meta: { title: '查看restApi', noCache: true }
          }
        ]
      },
      {
        path: 'binding',
        component: () => import('@/views/black'),
        name: 'bindings',
        meta: { title: '比赛绑定', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/match/matchinfo/binding/list'),
            name: 'match_list',
            meta: { title: '比赛绑定', noCache: true }
          },
          {
            path: 'update',
            component: () => import('@/views/gamecenter/match/matchinfo/binding/update'),
            name: 'match_update',
            meta: { title: '各游戏项目整体更新设置', noCache: true },
            hidden: true
          },
          {
            path: 'batchadd',
            component: () => import('@/views/gamecenter/match/matchinfo/binding/batchadd'),
            name: 'player_batchadd',
            meta: { title: '相似绑定', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'manageLog',
        component: () => import('@/views/black'),
        name: 'manageLog',
        meta: { title: '日志分拣管理', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/match/matchinfo/manageLog/list'),
            name: 'log_list',
            meta: { title: '日志分拣管理', noCache: true }
          },
          {
            path: 'add',
            component: () => import('@/views/gamecenter/match/matchinfo/manageLog/add'),
            name: 'log_add',
            meta: { title: '新建', noCache: true },
            hidden: true
          },
          {
            path: 'edit',
            component: () => import('@/views/gamecenter/match/matchinfo/manageLog/edit'),
            name: 'log_edit',
            meta: { title: '编辑', noCache: true },
            hidden: true
          },
          {
            path: 'normData',
            component: () => import('@/views/gamecenter/match/matchinfo/manageLog/normData'),
            name: 'normData',
            meta: { title: '数据转换详情', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'scoket',
        component: () => import('@/views/black'),
        name: 'scoket',
        meta: { title: 'scoket信息管理', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/match/matchinfo/scoket/list'),
            name: 'scoket_list',
            meta: { title: 'scoket信息管理', noCache: true }
          },
          {
            path: 'add',
            component: () => import('@/views/gamecenter/match/matchinfo/scoket/add'),
            name: 'scoket_add',
            meta: { title: '新建', noCache: true },
            hidden: true
          },
          {
            path: 'edit',
            component: () => import('@/views/gamecenter/match/matchinfo/scoket/edit'),
            name: 'scoket_edit',
            meta: { title: '编辑', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'csgo_info',
        component: () => import('@/views/black'),
        name: 'csgo_info',
        meta: { title: 'csgo数据信息', inoCache: true },
        hidden: true,
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/match/matchinfo/csgo_info/list'),
            name: 'csgo_info_list',
            meta: { title: 'csgo数据信息', noCache: true },
            hidden: true
          },
          {
            path: 'list1',
            component: () => import('@/views/gamecenter/match/matchinfo/csgo_info/list1'),
            name: 'csgo_info_list1',
            meta: { title: 'csgo数据信息(测试)', noCache: true }
          }
        ]
      }
    ]
  },
  {
    path: '/gamecenter/dataManage',
    component: Layout,
    redirect: 'gamecenter/dataManage',
    name: 'gamecenter_dataManage',
    meta: {
      title: '元数据管理',
      icon: 'dataManage'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'heroLeague',
        component: () => import('@/views/black'),
        name: 'gamecenters_heroLeague',
        meta: { title: '英雄联盟', inoCache: true },
        children: [
          {
            path: 'herolmanage',
            component: () => import('@/views/black'),
            name: 'herolmanage',
            meta: { title: '英雄管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/herolmanage/list'),
                name: 'heroLeague_list',
                meta: { title: '英雄管理', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/herolmanage/add'),
                name: 'herolmanage_add',
                meta: { title: '新建英雄', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/herolmanage/edit'),
                name: 'herolmanage_edit',
                meta: { title: '编辑英雄', noCache: true },
                hidden: true
              },

              {
                path: 'update',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/herolmanage/update'),
                name: 'herolmanage__update',
                meta: { title: '更新', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/black'),
                name: 'banding',
                meta: { title: '绑定', inoCache: true },
                children: [
                  {
                    path: 'list',
                    component: () => import('@/views/gamecenter/dataManage/heroLeague/herolmanage/banding/list'),
                    name: 'banding_list',
                    meta: { title: '英雄绑定', noCache: true }
                    // hidden: false
                  },
                  {
                    path: 'batchadd',
                    component: () => import('@/views/gamecenter/dataManage/heroLeague/herolmanage/banding/batchadd'),
                    name: 'lol_batchadd',
                    meta: { title: '相似绑定', noCache: true },
                    hidden: true
                  }
                ]
              }
            ]
          },
          {
            path: 'ability',
            component: () => import('@/views/black'),
            name: 'ability_lol',
            meta: { title: '技能管理', inoCache: true },
            children: [
              {
                path: 'ability_list',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/ability/ability_list'),
                name: 'ability_list_lol',
                meta: { title: '技能管理', noCache: true }
                // hidden: true
              },
              {
                path: 'ability_add',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/ability/ability_add'),
                name: 'ability_add_lol',
                meta: { title: '新建英雄技能', noCache: true },
                hidden: true
              },
              {
                path: 'ability_edit',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/ability/ability_edit'),
                name: 'ability_edit_lol',
                meta: { title: '编辑英雄技能', noCache: true },
                hidden: true
              },
              {
                path: 'banding_ability',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/ability/banding_ability'),
                name: 'banding_ability_lol',
                meta: { title: '技能绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/ability/batchadd'),
                name: 'lol_ability_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          },

          {
            path: 'prop',
            component: () => import('@/views/black'),
            name: 'prop',
            meta: { title: '道具管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/prop/list'),
                name: 'heroLeague_props',
                meta: { title: '道具管理', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/prop/add'),
                name: 'prop_add',
                meta: { title: '新建道具', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/prop/edit'),
                name: 'heroLeague_prop_edit',
                meta: { title: '编辑道具', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/prop/banding'),
                name: 'heroLeague_prop_banding',
                meta: { title: '道具绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/prop/batchadd'),
                name: 'lol_prop_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          },
          {
            path: 'summoner',
            component: () => import('@/views/black'),
            name: 'summoner',
            meta: { title: '召唤师技能管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/summoner/list'),
                name: 'summoner_list',
                meta: { title: '召唤师技能管理', noCache: true }
                // hidden: false
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/summoner/banding'),
                name: 'summoner_banding',
                meta: { title: '召唤师技能绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/summoner/add'),
                name: 'summoner_add',
                meta: { title: '新建召唤师', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/summoner/edit'),
                name: 'summoner_edit',
                meta: { title: '编辑召唤师', noCache: true },
                hidden: true
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/summoner/batchadd'),
                name: 'lol_summoner_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          },
          {
            path: 'symbol',
            component: () => import('@/views/black'),
            name: 'symbol',
            meta: { title: '符文管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/symbol/list'),
                name: 'symbol_list',
                meta: { title: '符文管理', noCache: true }
                // hidden: false
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/symbol/banding'),
                name: 'symbol_banding',
                meta: { title: '符文绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/symbol/add'),
                name: 'symbol_add',
                meta: { title: '新建符文', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/symbol/edit'),
                name: 'symbol_edit',
                meta: { title: '编辑符文', noCache: true },
                hidden: true
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/heroLeague/symbol/batchadd'),
                name: 'lol_symbol_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          }
        ]
      },
      {
        path: 'dota2',
        component: () => import('@/views/black'),
        name: 'dota2',
        meta: { title: '刀塔2', inoCache: true },
        children: [
          {
            path: 'herolmanage',
            component: () => import('@/views/black'),
            name: 'heroLeague',
            meta: { title: '英雄管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/dota2/herolmanage/list'),
                name: 'dota2_list',
                meta: { title: '英雄管理', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/dota2/herolmanage/add'),
                name: 'dota2_herolmanage_add',
                meta: { title: '新建英雄', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/dota2/herolmanage/edit'),
                name: 'herolmanage__edit',
                meta: { title: '编辑英雄', noCache: true },
                hidden: true
              },
              {
                path: 'update',
                component: () => import('@/views/gamecenter/dataManage/dota2/herolmanage/update'),
                name: 'herolmanage_update',
                meta: { title: '更新', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/black'),
                name: 'banding',
                meta: { title: '绑定', inoCache: true },
                children: [
                  {
                    path: 'list',
                    component: () => import('@/views/gamecenter/dataManage/dota2/herolmanage/banding/list'),
                    name: 'dota2_banding_list',
                    meta: { title: '英雄绑定', noCache: true }
                    // hidden: false
                  },
                  {
                    path: 'batchadd',
                    component: () => import('@/views/gamecenter/dataManage/dota2/herolmanage/banding/batchadd'),
                    name: 'dota2_batchadd',
                    meta: { title: '相似绑定', noCache: true },
                    hidden: true
                  }
                ]
              }
            ]
          },
          {
            path: 'ability',
            component: () => import('@/views/black'),
            name: 'ability',
            meta: { title: '技能管理', inoCache: true },
            children: [
              {
                path: 'ability_list',
                component: () => import('@/views/gamecenter/dataManage/dota2/ability/ability_list'),
                name: 'ability_list',
                meta: { title: '技能管理', noCache: true }
                // hidden: true
              },
              {
                path: 'ability_add',
                component: () => import('@/views/gamecenter/dataManage/dota2/ability/ability_add'),
                name: 'ability_add',
                meta: { title: '新建技能', noCache: true },
                hidden: true
              },
              {
                path: 'ability_edit',
                component: () => import('@/views/gamecenter/dataManage/dota2/ability/ability_edit'),
                name: 'ability_edit',
                meta: { title: '编辑技能', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/dota2/ability/banding'),
                name: 'ability_banding',
                meta: { title: '技能绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/dota2/ability/batchadd'),
                name: 'dota_ability_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          },
          {
            path: 'talent',
            component: () => import('@/views/black'),
            name: 'talent',
            meta: { title: '天赋管理', inoCache: true },
            children: [
              {
                path: 'talent_list',
                component: () => import('@/views/gamecenter/dataManage/dota2/talent/talent_list'),
                name: 'talent_list',
                meta: { title: '天赋管理', noCache: true }
                // hidden: true
              },
              {
                path: 'talent_add',
                component: () => import('@/views/gamecenter/dataManage/dota2/talent/talent_add'),
                name: 'talent_add',
                meta: { title: '新建天赋', noCache: true },
                hidden: true
              },
              {
                path: 'talent_edit',
                component: () => import('@/views/gamecenter/dataManage/dota2/talent/talent_edit'),
                name: 'talent_edit',
                meta: { title: '编辑天赋', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/dota2/talent/banding'),
                name: 'talent_banding',
                meta: { title: '天赋绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/dota2/talent/batchadd'),
                name: 'dota_talent_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          },
          {
            path: 'prop',
            component: () => import('@/views/black'),
            name: 'gamecenter_prop1',
            meta: { title: '道具管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/dota2/prop/list'),
                name: 'heroLeague_prop_list',
                meta: { title: '道具管理', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/dota2/prop/add'),
                name: 'dota2_prop_add',
                meta: { title: '新建道具', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/dota2/prop/edit'),
                name: 'prop_edit',
                meta: { title: '编辑道具', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/dota2/prop/banding'),
                name: 'prop_banding',
                meta: { title: '道具绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/dota2/prop/batchadd'),
                name: 'dota_prop_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          }
        ]
      },
      {
        path: 'csgo',
        component: () => import('@/views/black'),
        name: 'csgo',
        meta: { title: '反恐精英', inoCache: true },
        children: [
          {
            path: 'map',
            component: () => import('@/views/black'),
            name: 'map',
            meta: { title: '地图管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/csgo/map/list'),
                name: 'map_list',
                meta: { title: '地图管理', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/csgo/map/add'),
                name: 'map_add',
                meta: { title: '新建地图', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/csgo/map/edit'),
                name: 'map__edit',
                meta: { title: '编辑地图', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/csgo/map/banding'),
                name: 'map_banding_list',
                meta: { title: '地图绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/csgo/map/batchadd'),
                name: 'csgo_map_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          },
          {
            path: 'weapon',
            component: () => import('@/views/black'),
            name: 'weapon',
            meta: { title: '武器管理', inoCache: true },
            children: [
              {
                path: 'list',
                component: () => import('@/views/gamecenter/dataManage/csgo/weapon/list'),
                name: 'weapon_list',
                meta: { title: '武器管理', noCache: true }
                // hidden: false
              },
              {
                path: 'add',
                component: () => import('@/views/gamecenter/dataManage/csgo/weapon/add'),
                name: 'weapon_add',
                meta: { title: '新建武器', noCache: true },
                hidden: true
              },
              {
                path: 'edit',
                component: () => import('@/views/gamecenter/dataManage/csgo/weapon/edit'),
                name: 'weapon__edit',
                meta: { title: '编辑武器', noCache: true },
                hidden: true
              },
              {
                path: 'banding',
                component: () => import('@/views/gamecenter/dataManage/csgo/weapon/banding'),
                name: 'weapon_banding_list',
                meta: { title: '武器绑定', noCache: true }
                // hidden: false
              },
              {
                path: 'batchadd',
                component: () => import('@/views/gamecenter/dataManage/csgo/weapon/batchadd'),
                name: 'csgo_weapon_batchadd',
                meta: { title: '相似绑定', noCache: true },
                hidden: true
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/gamecenter/update',
    component: Layout,
    redirect: 'gamecenter/update',
    name: 'gamecenter_update',
    meta: {
      title: '数据更新',
      icon: 'update'
      // roles: ['gamecenter']
    },
    children: [
      {
        path: 'bindingInfo',
        component: () => import('@/views/gamecenter/update/bindingInfo'),
        name: 'bindingInfo',
        meta: { title: '查看详情', noCache: true },
        hidden: true
      },
      {
        path: 'updateTemplate',
        component: () => import('@/views/black'),
        name: 'updateTemplate',
        meta: { title: '数据更新默认设置', inoCache: true },
        children: [
          {
            path: 'template',
            component: () => import('@/views/gamecenter/update/updateTemplate/template_info'),
            name: 'updateTemplate_template',
            meta: { title: '数据更新默认设置', noCache: true }
          },
          {
            path: 'default',
            component: () => import('@/views/gamecenter/update/updateTemplate/default'),
            name: 'updateTemplate_default',
            meta: { title: '设置默认值', noCache: true },
            hidden: true
          },
          {
            path: 'operation',
            component: () => import('@/views/gamecenter/update/updateTemplate/operation'),
            name: 'updateTemplate_operation',
            meta: { title: '操作记录', noCache: true },
            hidden: false
          },
          {
            path: 'template_style',
            component: () => import('@/views/gamecenter/update/updateTemplate/template_style'),
            name: 'updateTemplate_default',
            meta: { title: '模版样式', noCache: true },
            hidden: true
          }
        ]
      },
      {
        path: 'updateinfo',
        component: () => import('@/views/black'),
        name: 'updateinfo',
        meta: { title: '数据更新', inoCache: true },
        children: [
          {
            path: 'list',
            component: () => import('@/views/gamecenter/update/updateinfo/list'),
            name: 'updateinfo_list',
            meta: { title: '数据更新', noCache: true }
          },
          {
            path: 'info',
            component: () => import('@/views/gamecenter/update/updateinfo/info'),
            name: 'updateinfo_info',
            meta: { title: '编辑', noCache: true },
            hidden: true
          }
        ]
      }
    ]
  },
  // {
  //   path: '/gamecenter/admin',
  //   component: Layout,
  //   name: 'manage',
  //   meta: {
  //     title: '接口管理员',
  //     icon: 'admin'
  //   },
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/gamecenter/admin/index'),
  //       name: 'index',
  //       meta: { title: '接口管理员', noCache: true }
  //     },
  //     {
  //       path: 'info',
  //       component: () => import('@/views/gamecenter/admin/info'),
  //       name: 'manage_info',
  //       meta: { title: '管理员详情', noCache: true },
  //       hidden: true
  //     },
  //     {
  //       path: 'add',
  //       component: () => import('@/views/gamecenter/admin/add'),
  //       name: 'manage_add',
  //       meta: { title: '新增', noCache: true },
  //       hidden: true
  //     },
  //     {
  //       path: 'edit',
  //       component: () => import('@/views/gamecenter/admin/edit'),
  //       name: 'manage_edit',
  //       meta: { title: '修改', noCache: true },
  //       hidden: true
  //     }
  //   ]
  // },
  {
    path: '/gamecenter/task',
    component: Layout,
    name: 'task',
    meta: {
      title: '任务管理',
      icon: 'task'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/gamecenter/task/list'),
        name: 'task_list',
        meta: { title: '任务管理', noCache: true }
      },
      {
        path: 'info',
        component: () => import('@/views/gamecenter/task/info'),
        name: 'task_info',
        meta: { title: '详情', noCache: true },
        hidden: true
      },
      {
        path: 'dataInspect',
        component: () => import('@/views/gamecenter/task/dataInspect'),
        name: 'dataInspect',
        meta: { title: '数据检查', noCache: true }
      },
      {
        path: 'debug',
        component: () => import('@/views/gamecenter/task/debug'),
        name: 'debug',
        meta: { title: '数据更新debug', noCache: true }
      },
      {
        path: 'dataRest',
        component: () => import('@/views/gamecenter/task/dataRest'),
        name: 'dataRest',
        meta: { title: '数据抓取', noCache: true }
      }
    ]
  },
  {
    path: '/gamecenter/manage',
    component: Layout,
    name: 'manage',
    meta: {
      title: '管理员管理',
      icon: 'manage',
      roles: ['管理员']
    },
    children: [
      {
        path: 'index',
        component: () => import('@/views/gamecenter/manage/index'),
        name: 'index',
        meta: { title: '管理员管理', icon: 'link', noCache: true }
      },
      {
        path: 'info',
        component: () => import('@/views/gamecenter/manage/info'),
        name: 'manage_info',
        meta: { title: '管理员详情', icon: 'link', noCache: true },
        hidden: true
      },
      {
        path: 'add',
        component: () => import('@/views/gamecenter/manage/add'),
        name: 'manage_add',
        meta: { title: '新增', icon: 'link', noCache: true },
        hidden: true
      },
      {
        path: 'edit',
        component: () => import('@/views/gamecenter/manage/edit'),
        name: 'manage_edit',
        meta: { title: '修改', icon: 'link', noCache: true },
        hidden: true
      }
    ]
  }
]
export default gamecenterRouters
