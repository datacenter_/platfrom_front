import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login', '/auth-redirect'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  NProgress.start()
  // 调试导航
  // addView(to.path).then().catch()
  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  if (hasToken) {
    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/' })
      NProgress.done()
    } else {
      // determine whether the user has obtained his permission roles through getInfo
      const hasRoles = store.getters.roles && store.getters.roles.length > 0
      if (hasRoles) {
        next()
      } else {
        try {
          // get user info
          // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
          const { roles, authList } = await store.dispatch('user/getInfo')

          // generate accessible routes map based on roles
          const accessRoutes = await store.dispatch('permission/generateRoutesWithAuth', { roles: roles, authList: authList })
          // 如果是管理员，这里判断是否有读权限，如果没有，干掉
          // dynamically add accessible routes
          router.addRoutes(accessRoutes)
          store.dispatch('optionUse/getCountry')
          store.dispatch('optionUse/getGames')
          store.dispatch('optionUse/getClan')
          store.dispatch('optionUse/getEnum')
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({ ...to, replace: true })
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      console.log('console.log(to.path)')
      console.log(to.path)
      // next(`/login?redirect=${to.path}`)
      // NProgress.done()
      // 如果是前端的，则不强制登录，反之，跳转到后端登录页
      var reg = new RegExp('^/homepage')
      if (reg.test(to.path)) {
        console.log('继续呀')
        next()
      } else {
        next(`/login?redirect=${to.path}`)
        NProgress.done()
      }
      // other pages that do not have permission to access are redirected to the login page.
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
