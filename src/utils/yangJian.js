const yangJian = {
  all_change_string(data) {
    function tmp(info) {
      for (var item in info) {
        if (info[item] && typeof info[item] === 'object') {
          info[item] = tmp(info[item])
        } else if (!info[item]) {
          info[item] = ''
        } else {
          info[item] = '' + info[item]
        }
      }
      return info
    }
    return tmp(data)
  },
  tmp_toString(key) {
    if (!key) {
      return ''
    } else {
      return '' + key
    }
  },
  tmp_nmber(key) {
    if (!key) {
      return ''
    } else {
      return 1 * key
    }
  },
  // 将秒转换成时间
  sec_format_tiem(value) {
    if (value) {
      var isTrue = false
      if (value<0) {
        isTrue = true
        value = Math.abs(value)
      }
      var ss = parseInt(value)// 秒
      var mm = 0// 分
      var hh = 0// 小时
      if (ss > 60) {
        mm = parseInt(ss / 60)
        ss = parseInt(ss % 60)
      }
      if (mm > 60) {
        hh = parseInt(mm / 60)
        mm = parseInt(mm % 60)
      }
      var result = ('00' + parseInt(ss)).slice(-2)
      if (mm > 0) {
        result = ('00' + parseInt(mm)).slice(-2) + ':' + result
      } else {
        result = '00:' + result
      }
      if (hh > 0) {
        result = ('00' + parseInt(hh)).slice(-2) + ':' + result
      } else {
        result = '00:' + result
      }
      if (isTrue) {
        return '-'+result
      }else {
        return result
      }
    } else {
      return value
    }
  },
  // 将时间转换成秒
  time_format_sec(time) {
    if (time) {
      var str = time
      var arr = str.split(':')
      var hs = parseInt(arr[0] * 3600)
      var ms = parseInt(arr[1] * 60)
      var ss = parseInt(arr[2])
      var seconds = hs + ms + ss
      return seconds
    } else {
      return ''
    }
  },
  slug_rule(data) {
    if (data) {
      const tmp = /[ -]/g
      var info = data.replace(tmp, '_')
      var info1 = info.toLowerCase()
      const tmp1 = /[\u4e00-\u9fa5^]/g
      var info2 = info1.replace(tmp1, '')
      const pattern = /[^\w^\s^\u4e00-\u9fa5]/gi
      var info3 = info2.replace(pattern, '')
      return info3
    } else {
      return ''
    }
  },
  // 匹配颜色高亮
  string_html(str, key) {
    var reg = new RegExp('(' + key + ')', 'i')
    var newstr = str.replace(reg, "<font style='background:#ff0;color: red;font-weight: bold '>$1</font>")
    return newstr
  },
  // get_platform_list() {
  //   const params = {}
  //   platform_list(params).then(req => {
  //     this.options = req.data
  //   })
  // },
  // 只能输入正整数
  integer_proving(data) {
    data = data.replace(/[^\.\d]/g, '')
    data = data.replace('.', '')
    return data
  },
  // 只能输入正整数和字母
  num_letter(data) {
    console.log('datawwsd')
    data = data.replace(/[^\.\w]/g, '')
    console.log(data)
    data = data.replace('.', '')
    return data
  },
  // 时间转换
  dateStr(d, sign) {
    // 如果没有传递符号，给一个默认的符号
    if (!sign) {
      sign = '-'
    }
    // 获取d里面年月日时分秒
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    const sun = d.getDate()
    const hours = d.getHours()
    const minutes = d.getMinutes()
    const seconds = d.getSeconds()

    return (
      year +
      sign +
      this.mendZero(month) +
      sign +
      this.mendZero(sun) +
      ' ' +
      this.mendZero(hours) +
      ':' +
      this.mendZero(minutes) +
      ':' +
      this.mendZero(seconds)
    )
  },
  // 时间转换补零
  mendZero(num) {
    return (num = num < 10 ? '0' + num : num)
  },
  // 转换百分比
  format(point) {
    const formated = Number(point * 100).toFixed(2)
    return `${formated}%`
  },
  // 时间格式化
  dateFormat(time) {
    if (time) {
      var date=new Date(time);
      var year=date.getFullYear();
      /* 在日期格式中，月份是从0开始的，因此要加0
       * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
       * */
      var month= date.getMonth()+1<10 ? "0"+(date.getMonth()+1) : date.getMonth()+1;
      var day=date.getDate()<10 ? "0"+date.getDate() : date.getDate();
      var hours=date.getHours()<10 ? "0"+date.getHours() : date.getHours();
      var minutes=date.getMinutes()<10 ? "0"+date.getMinutes() : date.getMinutes();
      var seconds=date.getSeconds()<10 ? "0"+date.getSeconds() : date.getSeconds();
      // 拼接
      return year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
    }else {
      return ''
    }
  },
  // JS取深度对象中的值
  safeProps(fn, defaultVal) {
    try {
      return fn();
    } catch (e) {
      return defaultVal;
    }
  },
  formatTime(time, fmt) {
    if (!time) return '';
    else {
      const date = new Date(time);
      const o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'H+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds(),
        'q+': Math.floor((date.getMonth() + 3) / 3),
        S: date.getMilliseconds(),
      };
      if (/(y+)/.test(fmt))
        fmt = fmt.replace(
          RegExp.$1,
          (date.getFullYear() + '').substr(4 - RegExp.$1.length)
        );
      for (const k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
          fmt = fmt.replace(
            RegExp.$1,
            RegExp.$1.length === 1
              ? o[k]
              : ('00' + o[k]).substr(('' + o[k]).length)
          );
        }
      }
      return fmt;
    }
  }
}
export default yangJian
