FROM registry.cn-beijing.aliyuncs.com/bcbase/front_base:1.0.0
COPY . /var/www/admin_front
RUN cd /var/www/admin_front ;\
npm run build:prod
EXPOSE 80 443
